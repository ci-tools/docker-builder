# Build a static jq binary
FROM alpine:3.10 AS JQbuilder
WORKDIR /workdir
RUN apk update && apk add --no-cache git autoconf automake libtool build-base
RUN git clone https://github.com/stedolan/jq.git
WORKDIR /workdir/jq
RUN git submodule update --init \
  && autoreconf -fi \
  && ./configure --disable-docs --disable-maintainer-mode --with-oniguruma \
  && make -j8 LDFLAGS=-all-static \
  && strip jq \
  && chmod 755 jq


# Build our final image
FROM gcr.io/kaniko-project/executor:v1.23.2-debug

ENTRYPOINT []

# Try to handle the auth file automatically
COPY auth.sh /kaniko/auth.sh

# Install https://github.com/estesp/manifest-tool
COPY --from=mplatform/manifest-tool:v2.1.7 /manifest-tool /kaniko/

# Install https://github.com/google/go-containerregistry/tree/main/cmd/crane (v0.19.0)
COPY --from=gcr.io/go-containerregistry/crane:c195f151efe3369874c72662cd69ad43ee485128 /ko-app/crane /kaniko/

# Install jq
COPY --from=JQbuilder /workdir/jq/jq /kaniko/

# Install Curl
COPY --from=tarampampam/curl:8.6.0 /bin/curl /kaniko/
ENV CURL_CA_BUNDLE /kaniko/ssl/certs/ca-certificates.crt

# Install Bash
RUN export ARCH=$(uname -m) \
  && curl -sSLo /bin/bash "https://github.com/robxu9/bash-static/releases/download/5.2.015-1.2.3-2/bash-linux-$ARCH" \
  && chmod +x /bin/bash
ENV PATH /bin:/usr/local/bin:/kaniko:/busybox
