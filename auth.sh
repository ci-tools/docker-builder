#!/bin/sh

echo_info () {
    echo -e "\e[32m$*\e[0m"
}

# Use the 'legacy' variables if they're defined
export CI_REGISTRY="${REGISTRY:-$CI_REGISTRY}"
export CI_REGISTRY_USER="${REGISTRY_USERNAME:-$CI_REGISTRY_USER}"
export CI_REGISTRY_PASSWORD="${REGISTRY_PASSWORD:-$CI_REGISTRY_PASSWORD}"

# If we have the right environment variables already, create the auth file automatically
if [[ ! -z "${CI_REGISTRY}" && ! -z "${CI_REGISTRY_USER}" && ! -z "${CI_REGISTRY_PASSWORD}" ]]; then
    echo_info "Creating auth file for registry ${CI_REGISTRY}"
    echo "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}" > /kaniko/.docker/config.json
else
    echo
    echo "Define CI_REGISTRY, CI_REGISTRY_USER and CI_REGISTRY_PASSWORD to automatically create Docker auth file, or create the auth file manually:"
    echo '  echo "{\"auths\":{\"my_registry.com\":{\"username\":\"myuser\",\"password\":\"mypass\"}}}" > /kaniko/.docker/config.json'
    echo
fi
