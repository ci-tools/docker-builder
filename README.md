# Docker Image Builder

This repository provides a pipeline to build multi-arch (currently `x86_64` and `aarch64`)
Docker images.

## Minimal usage example

In your repository with a `Dockerfile`, add a job like the following to your `.gitlab-ci.yml` file:

```yaml
build_image:
  stage: build
  trigger:
    include:
      - project: 'ci-tools/docker-builder'
        file: '/ci.yml'
    strategy: depend
  variables:
    TO: "${CI_REGISTRY_IMAGE}/myimage:latest"
```

This job will trigger a child pipeline that will build your Docker image for both architectures
and then merge them into a single multi-arch image.

## Configuration parameters

The following environment variables can be passed to the child pipeline to control its behaviour:

* `TO`: The image name to create. Defaults to `$CI_REGISTRY_IMAGE`. If no tag is specified, the `$CI_COMMIT_SHORT_SHA` will be used as a tag
* `CONTEXT_DIR`: The working directory where additional files referenced by the `Dockerfile` may be found. Defaults to `.`.
* `DOCKER_FILE`: Name of the Dockerfile. Defaults to `Dockerfile`.
* `BUILD_ARG`: A build arg as a string with format `key=value` to pass while building the image. You can also pass multiple build args at once with the environment variables `BUILD_ARG_1`, `BUILD_ARG_2`, etc. The value portion of the build args will be interpreted by a shell at build time.
* `BUILD_X86_64`: Build for `x86_64`. Defaults to `"true"` (note the quotes).
* `BUILD_AARCH64`: Build for `aarch64`. Defaults to `"true"` (note the quotes).
* `PARENT_PIPELINE_ID`: Pipeline ID of the parent pipeline (ie. your pipeline). No default, set to `$CI_PIPELINE_ID` if you need to pass artifacts to the child pipeline.
* `ARTIFACT_JOB`: Name of the job that exposes artifacts that the child pipeline needs. No default, set this only if needed.
* `ARM_RUNNER_TAG`: Tag of ARM Gitlab CI runners. Defaults to `docker-aarch64`.
* `CI_REGISTRY`: Registry URL.
* `CI_REGISTRY_USER`: Registry username.
* `CI_REGISTRY_PASSWORD`: Registry password.
* `CACHE`: Enable caching of image layers. Defaults to `"true"`.
* `CACHE_TTL`: Cache timeout. Defaults to `168h` (1 week).
* `CACHE_REPO`: Remote repository for storage of cache layers. Defaults to `$CI_REGISTRY_IMAGE/cache`.

## `tools` Docker image

This repository also includes (and uses) a docker image that includes a set of tools for
dealing with docker images and registries. The tools include [Kaniko](https://github.com/GoogleContainerTools/kaniko),
[manifest-tool](https://github.com/estesp/manifest-tool), [Crane](https://github.com/google/go-containerregistry/tree/main/cmd/crane),
[curl](https://curl.se/) and [jq](https://stedolan.github.io/jq/).
You may also use this Docker image in your own pipelines to retag images or for anything else you might need.

The full path to the docker image is `gitlab-registry.cern.ch/ci-tools/docker-builder/tools`.

## Using artifacts

By default, child pipelines won't download any of the artifacts created by their parent pipeline.
If you want to pass artifacts to a child pipeline (say, to build a dynamically-generated docker image), you
can do so using the `PARENT_PIPELINE_ID` and `ARTIFACT_JOB` environment variables. Here's an example:

```yaml
prepare:
  stage: prepare
  script:
    - mkdir dynamic
    - echo "FROM gitlab-registry.cern.ch/linuxsupport/alma9-base" >> dynamic/Dockerfile
  artifacts:
    paths:
      - dynamic/
    expire_in: 1 week

build_artifacts:
  stage: build
  trigger:
    include:
      - project: 'ci-tools/docker-builder'
        file: '/ci.yml'
    strategy: depend
  variables:
    TO: "${CI_REGISTRY_IMAGE}/test_dynamic:latest"
    CONTEXT_DIR: dynamic
    DOCKER_FILE: dynamic/Dockerfile
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID
    ARTIFACT_JOB: prepare
```

The job `prepare` creates and exposes some artifacts (`dynamic/`), and the child pipeline is told
to download them (via the `PARENT_PIPELINE_ID` and `ARTIFACT_JOB` variables). `PARENT_PIPELINE_ID` must always be `$CI_PIPELINE_ID`
and `ARTIFACT_JOB` should contain the name of the job that generated the artifacts.

## Extended usage example

Here is a sample `.gitlab-ci.yml` of a project building a docker image, testing it and tagging it to latest.

```yaml
stages:
  - build
  - test
  - tag

build_image:
  stage: build
  trigger:
    include:
      - project: 'ci-tools/docker-builder'
        file: '/ci.yml'
    strategy: depend
  variables:
    TO: "${CI_REGISTRY_IMAGE}/myimage"
    CONTEXT_DIR: "container/"
    BUILD_ARG_1: BUILD_TIME=$(date)
    BUILD_ARG_2: COMMIT_SHORT_SHA=$CI_COMMIT_SHORT_SHA

.test_image:
  stage: test
  image:
    name: "${CI_REGISTRY_IMAGE}/myimage:$CI_COMMIT_SHORT_SHA"
  script:
    - uname -a

test_x86_64:
  extends: .test_image

test_aarch64:
  extends: .test_image
  tags:
    - docker-aarch64

tag_latest:
  stage: tag
  image: gitlab-registry.cern.ch/ci-tools/docker-builder/tools
  variables:
    # No need to clone the repo
    GIT_STRATEGY: none
  # We don't need artifacts from previous steps
  dependencies: []
  script:
    - source /kaniko/auth.sh
    - crane tag "$CI_REGISTRY_IMAGE/myimage:$CI_COMMIT_SHORT_SHA" "latest"
    - crane validate --remote "$CI_REGISTRY_IMAGE/myimage:latest"
  only:
    - master
    - main
```
